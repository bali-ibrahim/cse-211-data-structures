#include <fstream>
#include <typeinfo>
#include "person.h"

enum By
{
    Age,
    Name
};

template <class T>
class NodeDim2
{
public:
    NodeDim2()
    {
        next_name = next_age = nullptr;
    }
    NodeDim2(const T &el, NodeDim2<T> *n = nullptr, NodeDim2<T> *u = nullptr)
    {
        info = el;
        next_name = n;
        next_age = u;
    }
    T info;
    NodeDim2<T> *next_name, *next_age;
};

class PersonList
{
public:
    PersonList()
    {
        head_name = head_age = nullptr;
    }
    ~PersonList()
    {
    }
    void add(std::string name, int age);
    void add(const Person &);
    bool remove(const Person &);
    bool remove(std::string name);
    void update(const Person &);
    void update(std::string name, int age);
    void printByAge();
    void printByName();
    void loadFile(std::string filename);
    void saveToFileByAge(std::string filename);
    void saveToFileByName(std::string filename);

protected:
    NodeDim2<Person> *head_name, *head_age;

private:
    template <typename T>
    void add(NodeDim2<Person> *new_node, const T &t)
    {
        By by = _resolve(t);
        NodeDim2<Person> *h = *head(by);
        if (info(h, t) >= info(new_node, t))
        {
            *next(new_node, by) = h;
            // h = new_node is a segmentation fault at basic_string.h __lhs.size()
            *head(by) = new_node;
            return;
        }

        for (NodeDim2<Person> *node = h; node != nullptr; node = *next(node, by))
        {
            NodeDim2<Person> *next_node = *next(node, by);
            if (next_node == nullptr || info(next_node, t) >= info(new_node, t))
            {
                *next(new_node, by) = next_node;
                // next_node = new_node is a segmentation fault at basic_string.h __lhs.size()
                *next(node, by) = new_node;
                return;
            }
        }
    }

    NodeDim2<Person> *remove(const Person &p, const By &by)
    {
        NodeDim2<Person> *h = *head(by);
        if (h->info == p)
        {
            *head(by) = *next(h, by);
            return h;
        }
        for (NodeDim2<Person> *node = h; node != nullptr; node = *next(node, by))
        {
            NodeDim2<Person> *next_node = *next(node, by);
            if (next_node != nullptr && next_node->info == p)
            {
                *next(node, by) = *next(next_node, by);
                return next_node;
            }
        }
        return nullptr;
    }

    NodeDim2<Person> *find(const Person &p)
    {
        for (NodeDim2<Person> *node = head_name; node != nullptr; node = node->next_name)
        {
            if (node->info == p)
            {
                return node;
            }
        }
        return nullptr;
    }

    template <typename T, typename T2>
    void print(const T &t, const T2 &t2)
    {
        streamIn(std::cout, t, t2);
    }
    template <typename T, typename T2>
    void saveToFile(const std::string &filename, const T &t, const T2 &t2)
    {
        std::ofstream myfile;
        myfile.open(filename);
        streamIn(myfile, t, t2);
        myfile.close();
    }

    template <typename T3, typename T, typename T2>
    void streamIn(T3 &streamTo, const T &t, const T2 &t2)
    {
        By by = _resolve(t);
        for (NodeDim2<Person> *node = *head(by); node != nullptr; node = *next(node, by))
        {
            streamTo << info(node, t) << " " << info(node, t2) << std::endl;
        }
    }
    std::string readFile(std::string filename)
    {
        std::ifstream in(filename);
        std::string contents((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>());
        return contents;
    }

    bool isEmpty()
    {
        return (head_name == nullptr && head_age == nullptr);
    }

    template <typename T>
    By _resolve(const T &)
    {
        if (typeid(T) == typeid(int))
        {
            return Age;
        }
        else if (typeid(T) == typeid(std::string))
        {
            return Name;
        }
    }

    int info(NodeDim2<Person> *node, const int &)
    {
        return node->info.age;
    }

    std::string info(NodeDim2<Person> *node, const std::string &)
    {
        return node->info.name;
    }

    NodeDim2<Person> **next(NodeDim2<Person> *node, const By &by)
    {
        switch (by)
        {
        case Age:
            return &(node->next_age);
        case Name:
            return &(node->next_name);
        default:
            return nullptr;
        }
    }

    NodeDim2<Person> **head(const By &by)
    {
        // https://stackoverflow.com/a/29966177/7032856
        switch (by)
        {
        case Age:
            return &head_age;
        case Name:
            return &head_name;
        default:
            return nullptr;
        }
    }
};

void PersonList::add(const Person &p)
{
    NodeDim2<Person> *new_node = new NodeDim2<Person>(p);

    if (isEmpty())
    {
        head_name = head_age = new_node;
        return;
    }

    int _;
    add(new_node, _);

    std::string __;
    add(new_node, __);
}

void PersonList::add(std::string name, int age)
{
    Person p = Person(name, age);
    return add(p);
}

bool PersonList::remove(const Person &p)
{
    if (isEmpty())
        return false;

    NodeDim2<Person> *deleted_name = remove(p, Name);
    NodeDim2<Person> *deleted_age = remove(p, Age);
    bool deleted = (deleted_age == deleted_name) && (deleted_age != nullptr);
    delete deleted_age;
    deleted_age = nullptr;
    if (!deleted)
    {
        std::cout << p.name << " does not exist." << std::endl;
    }

    return deleted;
}

bool PersonList::remove(std::string name)
{
    Person p = Person(name);
    return remove(p);
}

void PersonList::update(const Person &p)
{
    remove(p);
    add(p);
}

void PersonList::update(std::string name, int age)
{
    Person p = Person(name, age);
    return update(p);
}

void PersonList::printByAge()
{
    int _;
    std::string __;
    print(_, __);
    std::cout << "*** end of list by age ***" << std::endl;
}

void PersonList::printByName()
{
    std::string _;
    int __;
    print(_, __);
    std::cout << "--- end of list by name ---" << std::endl;
}

void PersonList::loadFile(std::string filename)
{
    std::string contents = readFile(filename);

    char letter = contents[0];
    std::string token = "";
    Person p;
    for (int i = 0; letter != '\0'; letter = contents[++i])
    {
        if (letter == ' ')
        {
            p.name = token;
            token = "";
        }
        else if (letter == '\n')
        {
            p.age = std::stoi(token);
            add(p);
            token = "";
        }
        else
        {
            token += letter;
        }
    }
}

void PersonList::saveToFileByAge(std::string filename = "age.txt")
{
    int _;
    std::string __;
    saveToFile(filename, _, __);
}

void PersonList::saveToFileByName(std::string filename = "name.txt")
{
    std::string _;
    int __;
    saveToFile(filename, _, __);
}