#include <string>
using namespace std;

class Person
{
public:
    Person(string n = "", int a = 0)
    {
        // id = auto_increment++;
        age = a;
        name = n;
    }
    int age;
    string name;
    // int id;

    bool operator==(Person const &obj)
    {
        // // Two Person objects are the same if both their names and their ages match
        // return ((obj.name == name) && (obj.age == age));

        // Two Person objects are the same if their names are a match
        return (obj.name == name);
    }

private:
    // static int auto_increment;
};

// int Person::auto_increment = 0;