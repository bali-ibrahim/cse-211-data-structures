#include <string>
#include <fstream>
#include "person.h"

class PersonDB
{
public:
    PersonDB()
    {
    }

    void add(std::string name, int age)
    {
        Person p = Person(name, age);
        return add(p);
    }

    bool
    remove(std::string name)
    {
        Person p = Person(name);
        return remove(p);
    }
    void update(std::string name, int age)
    {
        Person p = Person(name, age);
        return update(p);
    }
    void printByAge()
    {
        int _;
        printBy(_);
    }
    void printByName()
    {
        std::string _;
        printBy(_);
    }
    void loadFile(std::string filename)
    {
        std::string contents = readFile(filename);

        char letter = contents[0];
        std::string token = "";
        Person p;
        for (int i = 0; letter != '\0'; letter = contents[++i])
        {
            if (letter == ' ')
            {
                p.name = token;
                token = "";
            }
            else if (letter == '\n')
            {
                p.age = std::stoi(token);
                add(p);
                token = "";
            }
            else
            {
                token += letter;
            }
        }
    }
    void saveToFileByAge(std::string filename = "age.txt")
    {
        int _;
        saveToFile(filename, _);
    }
    void saveToFileByName(std::string filename = "name.txt")
    {
        std::string _;
        saveToFile(filename, _);
    }

private:
    void add(Person &p)
    {
        populateAsc(p, p.age);
        populateAsc(p, p.name);
    }
    DoublyLinkedList<Person> *dll(std::string &)
    {
        return &name_asc;
    }
    DoublyLinkedList<Person> *dll(int &)
    {
        return &age_asc;
    }
    int value(Person &p, int &)
    {
        return p.age;
    }
    std::string value(Person &p, std::string &)
    {
        return p.name;
    }
    template <class T>
    void printBy(T &t)
    {
        DoublyLinkedList<Person> *cdll = dll(t);
        Person head = cdll->firstEl();
        for (bool head_again = false; !head_again; cdll->addToDLLTail(cdll->deleteFromDLLHead()), head_again = head == cdll->firstEl())
        {
            Person p = cdll->firstEl();
            std::cout << p.name << " " << p.age << std::endl;
        }
        std::cout << std::endl;
    }
    template <class T>
    void saveToFile(std::string filename, T &t)
    {
        DoublyLinkedList<Person> *cdll = dll(t);
        Person head = cdll->firstEl();
        std::ofstream myfile;
        myfile.open(filename);
        for (bool head_again = false; !head_again; cdll->addToDLLTail(cdll->deleteFromDLLHead()), head_again = head == cdll->firstEl())
        {
            Person p = cdll->firstEl();
            myfile << p.name << " " << p.age << std::endl;
        }
        myfile.close();
    }
    template <class T>
    void populateAsc(Person &p, T &t)
    {
        DoublyLinkedList<Person> pdll;
        DoublyLinkedList<Person> *cdll = dll(t);

        for (; !cdll->isEmpty(); pdll.addToDLLTail(cdll->deleteFromDLLHead()))
        {
            if (!pdll.find(p) && value(p, t) <= value(cdll->firstEl(), t))
            {
                pdll.addToDLLTail(p);
                break;
            }
        }

        if (!pdll.find(p))
        {
            pdll.addToDLLTail(p);
        }

        for (; !pdll.isEmpty(); cdll->addToDLLHead(pdll.deleteFromDLLTail()))
        {
        }
    }
    bool remove(Person &p)
    {
        return removeFromDLL(age_asc, p) && removeFromDLL(name_asc, p);
    }

    bool removeFromDLL(DoublyLinkedList<Person> &dll, Person &p)
    {
        bool done = false;
        DoublyLinkedList<Person> pdll;
        for (; !dll.isEmpty(); pdll.addToDLLTail(dll.deleteFromDLLHead()))
        {
            if (p == dll.firstEl())
            {
                dll.deleteFromDLLHead();
                done = true;
                break;
            }
        }
        // iterate back
        for (; !pdll.isEmpty(); dll.addToDLLHead(pdll.deleteFromDLLTail()))
        {
        }
        return done;
    }
    void update(Person &p)
    {
        remove(p);
        add(p);
    }

    std::string readFile(std::string filename)
    {
        std::ifstream in(filename);
        std::string contents((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>());
        return contents;
    }
    DoublyLinkedList<Person> age_asc;
    DoublyLinkedList<Person> name_asc;
};