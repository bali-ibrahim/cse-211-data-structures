#ifndef DOUBLY_LINKED_LIST
#define DOUBLY_LINKED_LIST

using namespace std;

template <class T>
class DLLNode
{
public:
    DLLNode()
    {
        next = prev = nullptr;
    }
    DLLNode(const T &el, DLLNode<T> *n = nullptr, DLLNode<T> *p = nullptr)
    {
        info = el;
        next = n;
        prev = p;
    }
    T info;
    DLLNode<T> *next, *prev;
};

template <class T>
class DoublyLinkedList
{
public:
    DoublyLinkedList()
    {
        head = tail = nullptr;
    }
    void addToDLLTail(const T &);
    T deleteFromDLLTail();
    ~DoublyLinkedList()
    {
        clear();
    }
    bool isEmpty()
    {
        return (head == nullptr);
    }
    void clear();
    void setToNull()
    {
        head = tail = nullptr;
    }
    void addToDLLHead(const T &);
    T deleteFromDLLHead();
    T &firstEl();
    T *find(const T &) const;

protected:
    DLLNode<T> *head, *tail;
    friend ostream &operator<<(ostream &out, const DoublyLinkedList<T> &dll)
    {
        for (DLLNode<T> *tmp = dll.head; tmp != nullptr; tmp = tmp->next)
            out << tmp->info << ' ';
        return out;
    }
};

template <class T>
void DoublyLinkedList<T>::addToDLLHead(const T &el)
{
    if (head != nullptr)
    {
        head = new DLLNode<T>(el, head, nullptr);
        head->next->prev = head;
    }
    else
        head = tail = new DLLNode<T>(el);
}

template <class T>
void DoublyLinkedList<T>::addToDLLTail(const T &el)
{
    if (tail != nullptr)
    {
        tail = new DLLNode<T>(el, nullptr, tail);
        tail->prev->next = tail;
    }
    else
        head = tail = new DLLNode<T>(el);
}

template <class T>
T DoublyLinkedList<T>::deleteFromDLLHead()
{
    T el = head->info;
    if (head == tail)
    { // if only one DLLNode on the list;
        delete head;
        head = tail = nullptr;
    }
    else
    { // if more than one DLLNode in the list;
        head = head->next;
        delete head->prev;
        head->prev = nullptr;
    }
    return el;
}

template <class T>
T DoublyLinkedList<T>::deleteFromDLLTail()
{
    T el = tail->info;
    if (head == tail)
    { // if only one DLLNode on the list;
        delete head;
        head = tail = nullptr;
    }
    else
    { // if more than one DLLNode in the list;
        tail = tail->prev;
        delete tail->next;
        tail->next = nullptr;
    }
    return el;
}

template <class T>
T *DoublyLinkedList<T>::find(const T &el) const
{
    // if (isEmpty())
    // {
    //     return nullptr;
    // }

    DLLNode<T> *tmp = head;
    for (; tmp != nullptr && !(tmp->info == el); // overloaded ==
         tmp = tmp->next)
        ;
    if (tmp == nullptr)
        return nullptr;
    else
        return &tmp->info;
}

template <class T>
T &DoublyLinkedList<T>::firstEl()
{
    return head->info;
}

template <class T>
void DoublyLinkedList<T>::clear()
{
    for (DLLNode<T> *tmp; head != nullptr;)
    {
        tmp = head;
        head = head->next;
        delete tmp;
    }
}

#endif
