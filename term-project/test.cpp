#include <iostream>
#include <time.h>
#include "PersonList.h"

string RandomString(int len)
{
    string str = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    string newstr;
    int pos;
    while (newstr.size() != len)
    {
        pos = ((rand() % (str.size() - 1)));
        newstr += str.substr(pos, 1);
    }
    return newstr;
}
void test()
{
    std::srand(time(NULL));

    PersonList db = PersonList();
    const int range = 255;
    const int str_len = 10;
    Person p;
    for (int i = 0; i < 10; ++i)
    {
        p = Person("A" + RandomString(str_len - 1), rand() % range);
        db.add(p);
    }

    db.add(RandomString(str_len), -1);
    db.add(RandomString(str_len), range + 1);
    db.update(p.name, -2);
    db.loadFile("input.txt");
    db.printByAge();
    db.printByName();
    db.saveToFileByAge();
    db.saveToFileByName();
}

void test2()
{
    PersonList asd;

    Person h = Person("Halil", 28);
    Person o = Person("Oktay", 2);

    asd.add(Person("Guldeniz", 33));
    asd.add(Person("Guney", 31));
    asd.add(h);
    asd.add(Person("Ibrahim", 27));
    asd.add(Person("Atifet", 54));
    asd.add(o);

    asd.printByName();
    asd.printByAge();

    // std::cout << "guney" << std::endl;
    asd.saveToFileByName();
    asd.saveToFileByAge();

    asd.remove(o);

    asd.remove(h);

    asd.printByName();
    asd.printByAge();
}

int main()
{
    test();
    // test2();

    return 0;
};